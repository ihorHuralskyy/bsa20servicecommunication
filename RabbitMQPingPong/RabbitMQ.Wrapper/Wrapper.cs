﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RabbitMQ.Wrapper
{
    public class Wrapper:IDisposable
    {

        private EventingBasicConsumer _consumer;
        private IConnection _connection;
        private IModel _channel;
        private string message;
        private string listenTo;
        private string sendTo;

        public IModel Channel
        {
            get
            {
                if (_channel == null)
                    _channel = _connection.CreateModel();

                return _channel;
            }
        }

        public Wrapper(string mes,string listenToQueue,string sendToQueue)
        {
            message = mes;
            listenTo = listenToQueue;
            sendTo = sendToQueue;
        }

        public void Start()
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare("PingPongExchange", ExchangeType.Direct);
            _channel.QueueDeclare(queue: "ping_queue",
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
            _channel.QueueDeclare(queue: "pong_queue",
                                durable: true,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);
            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            _channel.QueueBind("ping_queue", "PingPongExchange", "ping_queue");
            _channel.QueueBind("pong_queue", "PingPongExchange", "pong_queue");
            _consumer = new EventingBasicConsumer(_channel);
            _consumer.Received += ListenQueue;
            _channel.BasicConsume(queue: listenTo,
                                 autoAck: false,
                                 consumer: _consumer);
            this.SendMessageToQueue();
            Console.ReadLine();
        }

        public void ListenQueue(object sender, BasicDeliverEventArgs ar)
        {
            var body = ar.Body.ToArray();
            var mess = Encoding.UTF8.GetString(body);
            Console.WriteLine($"{DateTime.Now} - Received {mess}");
            Thread.Sleep(2500);
            _channel.BasicAck(deliveryTag: ar.DeliveryTag, multiple: false);
            this.SendMessageToQueue();
        }

        public void SendMessageToQueue()
        {
            var body = Encoding.UTF8.GetBytes(this.message);
            var properties = _channel.CreateBasicProperties();
            properties.Persistent = true;
            _channel.BasicPublish(exchange: "PingPongExchange",
                                 routingKey: sendTo,
                                 basicProperties: properties,
                                 body: body);
            Console.WriteLine($"\n{DateTime.Now} - Send {this.message}");

        }
        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}
