﻿using RabbitMQ.Client;
using System;
using System.Text;
using RabbitMQ.Wrapper;


namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper wrapper = new Wrapper("pong", "pong_queue", "ping_queue");
            wrapper.Start();

        }
    }
}
