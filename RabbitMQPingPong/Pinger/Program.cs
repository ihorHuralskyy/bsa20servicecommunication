﻿using RabbitMQ.Wrapper;
using System;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Wrapper wrapper = new Wrapper("ping", "ping_queue", "pong_queue");
            wrapper.Start();
        }
    }
}
